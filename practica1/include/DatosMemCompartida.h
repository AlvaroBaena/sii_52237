#pragma once 
#include "Esfera.h"
#include "Raqueta.h"

class DatosMemCompartida //La información a compartir se compone de:
{       
public:         
	Esfera esfera;//La posición de la pelota
	Raqueta raqueta1;//La posición de la raqueta1 
	Raqueta raqueta2;//La posición de la raqueta2
	int accion;//accion1, accion2; //La acción, que es la que debe decidir el bot: 1 arriba, 0 nada, -1 abajo
};
